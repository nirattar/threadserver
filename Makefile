# Googletest src (for headers)
GTEST_DIR = ../googletest/googletest

# Where to find user code.
USER_DIR = .

INCLUDE_PATH = .

# Flags passed to the preprocessor.
# Set Google Test's header directory as a system directory.(do not generate warnings)
CPPFLAGS += -isystem $(GTEST_DIR)/include -I$(INCLUDE_PATH)

# Flags passed to the C++ compiler.
CXXFLAGS += -g -Wall -std=c++11 -Wextra -pthread

# All Google Test headers.  Usually you shouldn't change this
# definition.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h

# depend on google test binaries
GTEST_BINARIES = ./gtest

# All tests produced by this Makefile.
TESTS = test_all

# House-keeping build targets.
all : thread_server_main

clean :
	rm -f $(TESTS) gtest.a gtest_main.a *.o thread_server_main

# Build user code
thread.o : $(USER_DIR)/threads/thread.cpp $(USER_DIR)/threads/thread.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/threads/thread.cpp
tcp_stream.o : $(USER_DIR)/network/tcp_stream.cpp $(USER_DIR)/network/tcp_stream.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/network/tcp_stream.cpp
tcp_acceptor.o : $(USER_DIR)/network/tcp_acceptor.cpp $(USER_DIR)/network/tcp_acceptor.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/network/tcp_acceptor.cpp
main.o: $(USER_DIR)/main.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/main.cpp
tcp_consumer_thread.o : $(USER_DIR)/thread_server/tcp_consumer_thread.cpp $(USER_DIR)/thread_server/tcp_consumer_thread.h $(GTEST_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/thread_server/tcp_consumer_thread.cpp

# Build tests.
# ...

thread_server_main :  thread.o tcp_stream.o tcp_acceptor.o main.o tcp_consumer_thread.o
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -lpthread $^ -o $@
