#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <mutex>

//a linked list node implementation
template <typename T> class ListNode 
{
public:

	//construct from key, value, and pointer to next node.
	ListNode(int key, const T& elem, ListNode<T>* next) :
		m_key(key), m_elem(elem), m_next(next)
	{
	}
	virtual ~ListNode(){};

	int m_key;
	T m_elem;
	ListNode<T>* m_next;
};

//a linked list node, with a lock associated with it.
template <typename T> class ListNodeLocked : public ListNode<T>
{
public:
	ListNodeLocked();
	virtual ~ListNodeLocked() {};
private:
	std::mutex m_mutex;
};

#endif //LINKED_LIST_H
