#ifndef SET_H
#define SET_H

//a set interface.
//a set contains elements of arbitrary type, 
//each identified by a unique integer key.
//(two elements with the same key cannot exist in the set.)

template <typename T> class Set 
{
public:
	Set() {};
	virtual ~Set() {};

	virtual bool Add(int key, T element) = 0;
	virtual bool Remove(int key) = 0;
	virtual bool Contains(int key) = 0;
};



#endif //SET_H
