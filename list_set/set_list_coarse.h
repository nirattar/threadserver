#ifndef SET_LIST_FINE_H
#define SET_LIST_FINE_H

#include "set.h"
#include "list_node.h"
#include "spdlog/spdlog.h"
#include <mutex>
#include <algorithm>
#include <climits>

//a COARSE-grained locking implementation of the set interface.
//the set is implemented as a singly-linked list, 
//while traversing the list is done by locking the whole structure for the duration
//of the operation.

template <typename T> 
class SetListCoarse : public Set<T>
{
public:
	SetListCoarse();
	virtual ~SetListCoarse();

	virtual bool Add(int key, T element);
	virtual bool Remove(int key);
	virtual bool Contains(int key);

private:
	std::pair<ListNode<T>*,ListNode<T>*> FindElem(int key);

	ListNode<T>* m_head;

	// A global lock for the structure.
	std::mutex m_mutex;

};

// IMPLEMENTATION
#include "set_list_coarse.cpp"


#endif //SET_LIST_FINE_H
