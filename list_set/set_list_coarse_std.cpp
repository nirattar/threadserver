

template <typename T>
SetListCoarse<T>::SetListCoarse() : 
	Set<T>(), 
	m_list(),
	m_mutex()
{
	spdlog::get("lg")->info("constructed STD-list-based set with coarse-grained locking.");
}

template <typename T>
SetListCoarse<T>::~SetListCoarse(){};

template <typename T>
bool SetListCoarse<T>::Add(int key, T element) 
{
	std::lock_guard<std::mutex> lock_guard(m_mutex);	//acquire lock, for duration of scope
	auto it = std::find_if(m_list.begin(), m_list.end(), 
					[=](const std::pair<int,T>& curr){ return curr.first == key;} );

	if(it == m_list.end())
	{
		//new element, insert.
		m_list.push_front(std::pair<int,T>(key,element));
		return true;
	}
	//existing element, do not update.
	return false;

}

template <typename T>
bool SetListCoarse<T>::Contains(int key)
{
	std::lock_guard<std::mutex> lock_guard(m_mutex);	//acquire lock, for duration of scope
	auto it = std::find_if(m_list.begin(), m_list.end(), 
					[=](const std::pair<int,T>& element){ return element.first == key;} );
	return (it != m_list.end());
}

template <typename T>
bool SetListCoarse<T>::Remove(int key) 
{
	std::lock_guard<std::mutex> lock_guard(m_mutex);	//acquire lock, for duration of scope
	//std::remove :  1. guaranteed to remove once (by the set properties)
	//2. when element did not originally exist, returns list's end.
	auto it = std::remove_if(m_list.begin(), m_list.end(), 
					[=](const std::pair<int,T>& curr){ return curr.first == key;} );
	return (it != m_list.end());
}
	




