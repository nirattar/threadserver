#ifndef SET_LIST_COARSE_STD_H
#define SET_LIST_COARSE_STD_H

#include "set.h"
#include "spdlog/spdlog.h"
#include <forward_list>
#include <utility>
#include <mutex>
#include <algorithm>

//an COARSE-grained locking implementation of the set interface. 
//implemented using an std::forward_list, with a global lock for the whole list.

template <typename T> 
class SetListCoarse : public Set<T>
{
public:
	SetListCoarse();
	virtual ~SetListCoarse();

	virtual bool Add(int key, T element);
	virtual bool Remove(int key);
	virtual bool Contains(int key);

private:
	std::forward_list<std::pair<int,T>> m_list;
	std::mutex m_mutex;
};

// IMPLEMENTATION
#include "set_list_coarse_std.cpp"


#endif //SET_H
