
template <typename T>
SetListFine<T>::SetListFine() : 
	Set<T>()
{
		// Tail sentinel
		auto tail = new ListNode<T>(INT_MAX, 0, nullptr);

		// Head setinel
		m_head = new ListNode<T>(INT_MIN, 0, tail);
}

template <typename T>
SetListFine<T>::~SetListFine()
{
	delete m_head;
}

template <typename T>
bool SetListFine<T>::Add(int key, T element) 
{
	// validate list in valid state.
	assert(m_head && m_head->m_next);

	// find a spot to insert the item : a consequtive pair of items 'prev' and 'curr',
	// s.t. prev.m_key < key < curr.m_key.
	auto prev = m_head;
	while(prev->m_next)
	{
		auto curr = prev->m_next; //curr already checked for nullity.
		if(curr->m_key == key) //found element with same key.
		{
			return false;
		}
		else if(key < curr->m_key )	//found first element with bigger key.
		{
			//found a spot for element, insert it (between prev and curr).
			auto item = new ListNode<T>(key, element, curr);
			prev->m_next = item;
			return true;
		}
		//advance to next element.
		prev = curr;
	}
	return false;

}

template <typename T>
bool SetListFine<T>::Contains(int key)
{
	return (FindElem(key).second);
}

//return previous element and element
template <typename T>
std::pair<ListNode<T>*,ListNode<T>*> SetListFine<T>::FindElem(int key)
{
	// validate list in valid state.
	assert(m_head && m_head->m_next);

	auto prev = m_head;
	while(prev->m_next)
	{
		auto curr = prev->m_next;
		if(curr->m_key==key /*curr already checked for nullity*/
			&& curr->m_key < INT_MAX /*avoid tail sentinel*/)
		{
			//found element, return item and its previous.
			return std::pair<ListNode<T>*,ListNode<T>*>(prev,curr);
		}
		//advance to next element.
		prev = curr;
	}
	return std::pair<ListNode<T>*,ListNode<T>*>(nullptr,nullptr);
}

template <typename T>
bool SetListFine<T>::Remove(int key) 
{
	// Find the element and its predecessor.
	auto result = FindElem(key);
	if(!result.second || !result.first)
	{
		return false;
	}

	// Element is found, delete it.
	auto prev = result.first;
	auto element = result.second;
	prev->m_next = element->m_next;	//both checked to be non-null.

	// Get rid of element storage.
	element->m_next = nullptr;
	delete element;
	return true;
}




