#ifndef SET_LIST_FINE_H
#define SET_LIST_FINE_H

#include "set.h"
#include "list_node.h"
#include "spdlog/spdlog.h"
#include <algorithm>
#include <climits>

//a FINE-grained locking implementation of the set interface.
//the set is implemented as a singly-linked list, 
//while traversing the list is done by locking 2 consecutive elements at a time.

template <typename T> 
class SetListFine : public Set<T>
{
public:
	SetListFine();
	virtual ~SetListFine();

	virtual bool Add(int key, T element);
	virtual bool Remove(int key);
	virtual bool Contains(int key);

private:
	ListNode<T>* m_head;
	std::pair<ListNode<T>*,ListNode<T>*> FindElem(int key);

};

// IMPLEMENTATION
#include "set_list_fine.cpp"


#endif //SET_LIST_FINE_H
