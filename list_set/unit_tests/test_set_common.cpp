#include "set.h"
#include "gtest/gtest.h"
#include "spdlog/spdlog.h"

void TestRemove(Set<const char*> * set)
{
	assert(set);
	
	EXPECT_TRUE(set->Add(5,"a"));
	EXPECT_TRUE(set->Add(9,"a"));
	EXPECT_TRUE(set->Add(3,"a"));
	EXPECT_TRUE(set->Add(7,"a"));
	
	//remove an existing element
	EXPECT_TRUE(set->Remove(3));
	//try removing a non-existing element
	EXPECT_FALSE(set->Remove(2));

	//check deleted element is not there anymore.
	EXPECT_FALSE(set->Contains(3));
	//try removing a non-existing element
	EXPECT_FALSE(set->Remove(3));

	//add after remove
	EXPECT_TRUE(set->Add(3,"b"));
	EXPECT_TRUE(set->Contains(3));

}

void TestAdd(Set<const char*> * set)
{
	EXPECT_TRUE(set->Add(5,"a"));
	EXPECT_FALSE(set->Add(5,"b"));
	EXPECT_TRUE(set->Add(2,"a"));
	EXPECT_FALSE(set->Add(5,"c"));
}

void TestContains(Set<const char*> * set)
{
	EXPECT_TRUE(set->Add(5,"a"));
	EXPECT_TRUE(set->Add(7,"a"));

	EXPECT_FALSE(set->Contains(1));
	EXPECT_TRUE(set->Contains(7));
	EXPECT_TRUE(set->Contains(5));
	
	EXPECT_TRUE(set->Add(1,"a"));
	EXPECT_TRUE(set->Contains(1));
}
