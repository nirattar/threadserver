#ifndef TEST_SET_COMMON_H
#define TEST_SET_COMMON_H
#include "set.h"

void TestRemove(Set<const char*> * set);

void TestAdd(Set<const char*> * set);

void TestContains(Set<const char*> * set);

#endif //TEST_SET_COMMON_H
