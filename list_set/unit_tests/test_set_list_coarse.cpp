/*
 * main.cpp
 * 
 * Copyright 2018 Nir Attar <nattar@ubuntu-nir>
 * 
 */


#include "test_set_common.h"
#ifndef SET_LIST_COARSE_USE_STD
	#include "set_list_coarse.h"
#else
	#include "set_list_coarse_std.h"
#endif
#include "gtest/gtest.h"
#include "spdlog/spdlog.h"

// Create a list-based set with coarse-grained locking.
TEST(TestSetListCoarse, Create)
{
	//Create logger: Create and return a shared_ptr to a multithreaded console logger.
	auto console = spdlog::stdout_logger_mt("lg");

	SetListCoarse<int> set1;
	SetListCoarse<float> set2;
	SetListCoarse<float*> set3;
}

//insert object twice will not succeed
TEST(TestSetListCoarse, Insert)
{
	SetListCoarse<const char*> set;
	TestAdd(&set);
}

//contains
TEST(TestSetListCoarse, Contains)
{
	SetListCoarse<const char*> set;
	TestContains(&set);
}

//remove
TEST(TestSetListCoarse, Remove)
{
	SetListCoarse<const char*> set;
	TestRemove(&set);
}


