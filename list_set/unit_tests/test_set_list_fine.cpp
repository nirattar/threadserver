/*
 * main.cpp
 * 
 * Copyright 2018 Nir Attar <nattar@ubuntu-nir>
 * 
 */

#include "test_set_common.h"
#include "set_list_fine.h"
#include "gtest/gtest.h"
#include "spdlog/spdlog.h"

// Create a list-based set with fine-grained locking.
TEST(TestSetListFine, Create)
{
	SetListFine<int> set1;
	SetListFine<float> set2;
	SetListFine<float*> set3;
}

//insert object twice will not succeed
TEST(TestSetListFine, Insert)
{
	SetListFine<const char*> set;
	TestAdd(&set);
}

//contains
TEST(TestSetListFine, Contains)
{
	SetListFine<const char*> set;
	TestContains(&set);
}

//remove
TEST(TestSetListFine, Remove)
{
	SetListFine<const char*> set;
	TestRemove(&set);
}


