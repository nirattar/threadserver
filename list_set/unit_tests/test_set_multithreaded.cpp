#ifndef SET_LIST_COARSE_USE_STD
	#include "set_list_coarse.h"
#else
	#include "set_list_coarse_std.h"
#endif
#include "gtest/gtest.h"
#include "spdlog/spdlog.h"

enum OperationType
{
	ADD,
	CONTAINS,
	REMOVE
};

struct SetOperation
{
	OperationType op;
	int value;
	std::string Print();
};

std::string SetOperation::Print()
{
	std::string result("");
	switch (op)
	{
		case ADD:
			result+= "ADD";
			break;
		case CONTAINS:
			result+= "CONTAINS";
			break;
		case REMOVE:
			result+= "REMOVE";
			break;
	}
	result += " ";
	result += std::to_string(value);
	return result;
}

// Encode and execute actions in "SetOperation" type
TEST(TestSetListMultithreaded, OperationTest1)
{
	SetListCoarse<int> set;

	// Construct an operation (Add, contains, remove).
	SetOperation Add7 = {ADD, 7};
	spdlog::get("lg")->info("constructed operation: {}", Add7.Print());

	// Do operation (Add, contains, remove) on the set.

	// Print operation results

}

// Make multiple threads contend on operations on a list-based set.
TEST(TestSetListMultithreaded, MultithreadedTest1)
{

	SetListCoarse<int> set;

	// Create x number of threads
	// For each thread:
		// Do any of the operations (Add, contains, remove) with random number.
		
		// Print operation results

}
