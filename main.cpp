/*
 * main.cpp
 * 
 * Copyright 2018 Nir Attar <nattar@ubuntu-nir>
 * 
 */

#include "network/tcp_acceptor.h"
#include "thread_server/tcp_work_item.h"
#include "thread_server/tcp_consumer_thread.h"
#include "work_queue/work_queue.h"
#include "spdlog/spdlog.h"

namespace spd = spdlog;

void DoAcceptLoop(WorkQueue<TCPWorkItem*>& wq)
{
	TCPAcceptor acceptor (8080, "192.168.1.1");
	bool result = acceptor.Init();
	if(!result)
	{
		spd::get("lg")->info("Failed to initialize listening TCP socket.");
		exit(1);
	}

	while(true)
	{
		// Accept a new client connection
		TCPStream * client = acceptor.Accept();
		if(!client)
		{
			spd::get("lg")->info("client is null.");
			continue;
		}

		// Make a new work item, enqueue it in work queue.
		// (Queue will now be owner of the pointer)
		auto clientItem = new TCPWorkItem(client);
		wq.addItem(clientItem);
	}
}

int main(int argc, char **argv)
{

	//Create logger: Create and return a shared_ptr to a multithreaded console logger.
	auto console = spd::stdout_logger_mt("lg");
	
	const unsigned NUM_WORKERS = 4;

	// create work queue and consumer threads.
	WorkQueue<TCPWorkItem*> wq;
	for (int i = 0; i < NUM_WORKERS; i++) 
	{
		TCPConsumerThread* consumer = new TCPConsumerThread(wq);
		if (!consumer) 
		{
			spd::get("lg")->error("Could not create TCPConsumerThread {}", i);
			exit(1);
		} 
		consumer->start();
	}
    
	// crate work items based on accepted connections
	DoAcceptLoop(wq);
	return 0;
}

