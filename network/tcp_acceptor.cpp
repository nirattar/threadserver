#include "tcp_acceptor.h"
#include "spdlog/spdlog.h"
#include <sys/types.h> /* See NOTES */
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <string>

namespace spd = spdlog;

TCPAcceptor::TCPAcceptor(unsigned port, const string& address) 
: m_bind_port(port), m_bind_address(address), m_listening_sd(-1)
{
}

TCPAcceptor::~TCPAcceptor()
{
	spd::get("lg")->info("TCPAcceptor destructor called. ");
	// Close listening socket, if it was created.
	if(m_listening_sd>=0 && close(m_listening_sd)!=0)
	{
		spd::get("lg")->info("failed closing listening socket {}", m_listening_sd);
	}
}

// run commands socket, bind and listen. 
// return error code.
bool TCPAcceptor::Init() 
{
	int status;
	struct addrinfo hints;
	struct addrinfo *localhost_addrinfo;  // will point to the results

	memset(&hints, 0, sizeof hints); // make sure the struct is empty
	hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
	hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

	if ((status = getaddrinfo(NULL, std::to_string(m_bind_port).c_str(), 
		&hints, &localhost_addrinfo)) != 0) 
	{
		spd::get("lg")->error("getaddrinfo error: {}\n", gai_strerror(status));
		return false;
	}

	// localhost_addrinfo now points to a linked list of 1 or more struct addrinfos

	// call 'socket' to get an initialized socket descriptor, for tcp.
	int result = socket(localhost_addrinfo->ai_family, localhost_addrinfo->ai_socktype, 
		localhost_addrinfo->ai_protocol);
	if (result==-1) 
	{
		spd::get("lg")->info("failed to create socket.");
		freeaddrinfo(localhost_addrinfo); // free the linked-list
		return false;
	}
	m_listening_sd = result;
	
	
	/*int bind(int sockfd, const struct sockaddr *addr,
		socklen_t addrlen);*/
	// call 'bind' to the socket to the address/port.
	result = bind(m_listening_sd, localhost_addrinfo->ai_addr, localhost_addrinfo->ai_addrlen);
	if (result==-1) 
	{
		spd::get("lg")->info("failed to bind.");
		freeaddrinfo(localhost_addrinfo); // free the linked-list
		return false;
	}  
	
	//call listen
	result = listen(m_listening_sd, SIZE_BACKLOG);
	if (result==-1) 
	{
		spd::get("lg")->info("failed to listen.");
		freeaddrinfo(localhost_addrinfo); // free the linked-list
		return false;
	}

	spd::get("lg")->info("listening!");
	freeaddrinfo(localhost_addrinfo); // free the linked-list

	return true;
}

TCPStream * TCPAcceptor::Accept()
{
	struct sockaddr_storage peer_addr;
	socklen_t addr_size = sizeof (peer_addr);

	// Blocks until new connection is ready. 
	int new_client_sd = accept(m_listening_sd, (struct sockaddr *)&peer_addr, &addr_size);
	if( new_client_sd == -1)
	{
		return NULL;
	}
	
	return new TCPStream(new_client_sd, *((struct sockaddr_in *)&peer_addr) );
}
