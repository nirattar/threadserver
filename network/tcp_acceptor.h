#include "tcp_stream.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string>

// TCP acceptor. listens to connections.
// once connection established, returns TCPStream.

// https://linux.die.net/man/7/ip

using std::string;

class TCPAcceptor 
{
public: 
	static const unsigned SIZE_BACKLOG = 10;

	TCPAcceptor(unsigned port, const string& address);
	virtual ~TCPAcceptor();

	// Initialize and start listening on port.
	bool Init();
	
	// Blocks until new connection is ready. 
	// Caller becomes owner of returned object.
	TCPStream * Accept();

private:
	unsigned	m_bind_port;
	string 		m_bind_address;
	//socket descriptor for listening socket.
	unsigned	m_listening_sd;
};
