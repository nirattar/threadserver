#include "tcp_stream.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace spd = spdlog;

TCPStream::~TCPStream() 
{
	spd::get("lg")->info("TCPStream destroyed. {}", (*this) );
	if(close(m_socket_fd)!=0)
	{
		spd::get("lg")->info("failed closing socket {}", m_socket_fd );
	}
	else 
	{
		spd::get("lg")->info("closed socket {}", m_socket_fd );
	}
}

TCPStream::TCPStream(int socket_fd, const struct sockaddr_in& peer_socket_address)
: m_socket_fd(socket_fd) /*FIXME: consume peer_socket_address*/
{

	// get information from sockaddr_in
	/*
	struct sockaddr_in {
		short int          sin_family;  // Address family, AF_INET
		unsigned short int sin_port;    // Port number
		struct in_addr     sin_addr;    // Internet address
		unsigned char      sin_zero[8]; // Same size as struct sockaddr
	};
	* struct in_addr {
		uint32_t s_addr; // that's a 32-bit int (4 bytes)
	};
	* */
	char ip4[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &(peer_socket_address.sin_addr), ip4, INET_ADDRSTRLEN);
	m_peer_ip = ip4;
	m_peer_port = peer_socket_address.sin_port;

	spd::get("lg")->info("TCPStream constructed. {}",  (*this) );

}

unsigned TCPStream::Read(void *buf, size_t len)
{
	//corresponds to: ssize_t recv(int sockfd, void *buf, size_t len, int flags);
	//recv returns the number of bytes received, or -1 if an error occurred. 
	//The return value will be 0 when the peer has performed an orderly shutdown.
	ssize_t result = recv(m_socket_fd, buf, len, 0);

	//TODO more error handling ?

	return result;
}


unsigned TCPStream::Write(const void *buf, size_t len)
{
	//corresponds to: ssize_t send(int sockfd, const void *buf, size_t len, int flags);
	ssize_t result = send(m_socket_fd, buf, len, 0);

	//TODO more error handling ?

	return result;
}

// Print stream details
::std::ostream& operator<<(::std::ostream& os, const TCPStream& stream)
{
	return os << "File descriptor: " << stream.getSockedDescriptor() << ::std::endl <<
	"Peer address: " << stream.m_peer_ip << ::std::endl <<
	"Peer port: " << std::to_string(stream.m_peer_port) << ::std::endl;  
}
