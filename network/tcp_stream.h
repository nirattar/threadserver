#ifndef TCP_STREAM_H
#define TCP_STREAM_H

#include "gtest/gtest_prod.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string>

// Provides an abstraction of a tcp stream.
// a stream is a context for an already open tcp connection.
// Instances of TCPStream will be created by a client or a server 
// (through classes TCPConnector or TCPAcceptor, respectively), 
// once connection has been established, storing the socket file descriptor.
// TCPStream will provide read/write operations on the connection,
// as well as hold information abouth other side of the connetion (peer).

// https://linux.die.net/man/7/ip

using std::string;

class TCPStream 
{

public:

	virtual ~TCPStream();

	friend class TCPAcceptor;
    //friend class TCPConnector;

	//corresponds to: ssize_t recv(int sockfd, void *buf, size_t len, int flags);
	unsigned Read(void *buf, size_t len);
	
	//corresponds to: ssize_t send(int sockfd, const void *buf, size_t len, int flags);
	unsigned Write(const void *buf, size_t len);
	
	string getPeerIP() const { return m_peer_ip; }
	unsigned getPeerPort() const { return m_peer_port; }

	friend ::std::ostream& operator<<(::std::ostream& os, const TCPStream& stream);

private:

	int m_socket_fd;
	string m_peer_ip;
	unsigned m_peer_port;
	int getSockedDescriptor() const {return m_socket_fd;}

	//make CTORs private
	TCPStream();
	TCPStream(int socket_fd, const struct sockaddr_in& peer_socket_address);
	TCPStream(const TCPStream& other); //copy

	//declare specific tests as friend
	FRIEND_TEST(TCPStreamTest, Init);

};
#endif //TCP_STREAM_H
