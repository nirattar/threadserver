// Step 1. Include necessary header files such that the stuff your
// test logic needs is declared.
//
// Don't forget gtest.h, which declares the testing framework.

#include "../network/tcp_acceptor.h"
#include "gtest/gtest.h"
#include <unistd.h>

#define MAX_SIZE_REQ 1024

namespace 
{

// Step 2. Use the TEST macro to define your tests.
//
// TEST has two parameters: the test case name and the test name.
// After using the macro, you should define your test logic between a
// pair of braces.  You can use a bunch of macros to indicate the
// success or failure of a test.  EXPECT_TRUE and EXPECT_EQ are
// examples of such macros.  For a complete list, see gtest.h.
//

TEST(TCPAcceptorTest, Init)
{
	TCPAcceptor acceptor (8080, "192.168.1.1");
	bool result = acceptor.Init();
	EXPECT_TRUE(result);
}



}  // namespace

// Step 3. Call RUN_ALL_TESTS() in main().
//
// We do this by linking in src/gtest_main.cc file, which consists of
// a main() function which calls RUN_ALL_TESTS() for us.
//
// This runs all the tests you've defined, prints the result, and
// returns 0 if successful, or 1 otherwise.
//
