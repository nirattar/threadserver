
/*
 * main.cpp
 * 
 * Copyright 2018 Nir Attar <nattar@ubuntu-nir>
 * 
 */

#include "tcp_acceptor.h"
#include <iostream>
#include <unistd.h>

using std::cout;
using std::endl;
#define MAX_SIZE_REQ 1024


void TestInit()
{
	TCPAcceptor acceptor (8080, "192.168.1.1");
	bool result = acceptor.Init();
	cout << "success? " << result << endl;
}

void TestAccept()
{
	TCPAcceptor acceptor (8080, "192.168.1.1");
	bool result = acceptor.Init();
	cout << "success? " << result << endl;
	
	TCPStream * client = acceptor.Accept();
	if(!client)
	{
		cout << "client is null." << endl;
	}
	delete(client);
}

void TestAcceptLoop()
{
	TCPAcceptor acceptor (8080, "192.168.1.1");
	bool result = acceptor.Init();
	cout << "success? " << result << endl;
	while(true)
	{
		TCPStream * client = acceptor.Accept();
		if(!client)
		{
			cout << "client is null." << endl;
			break;
		}
		
		//client is not null
		cout << "handle client " << client->getPeerIP() 
			<< "port " << client->getPeerPort() << endl;
		
		//read data from client
		char buf_req[MAX_SIZE_REQ];
		unsigned bytes_req = client->Read(buf_req, MAX_SIZE_REQ);
		cout << "read " << bytes_req << " bytes from client." << endl;
		// FIXME get rid of this
		printf("message: '%.*s'\n", bytes_req, buf_req);

		//echo request to client.
		client->Write(buf_req, bytes_req);

		// client handling is done, clear for next.
		delete(client);
	}
}

int main(int argc, char **argv)
{
	//Test init method
	TestInit();
	
	//Test init and accept.
	TestAccept();

	sleep(5);	//FIXME hack to allow previous acceptor to unbind properly.

	//Test serial (synchronuous) handling of clients.
	TestAcceptLoop();
	return 0;
}

