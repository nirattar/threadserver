# A simple script to simulate clients for testing the server. 
# clients will be run as child processes (hence, will run concurrently).
# Each client will make a series of short TCP connections to the server.
# (achieved using 'curl' HTTP client. Note: server is protocol agnostic ,
# simply reads from the connection and echos the content back.)

runJob()
{
	for i in `seq 10`; do
		curl http://localhost:8080/?seq=$i
	done
}

for i in `seq 20`; do 
	runJob "$i" &
done
