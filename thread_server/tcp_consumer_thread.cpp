#include "tcp_consumer_thread.h"
#include "spdlog/spdlog.h"

namespace spd = spdlog;

#define MAX_SIZE_REQ 1024

void * TCPConsumerThread::run()
{
	//dequeue items
	for (int i = 0;; i++)
	{
		spd::get("lg")->info("thread {}, loop {} - waiting for item...",
				getId(), i);

		auto workItem = m_queue.removeItem();
		if(!workItem)
		{
			spd::get("lg")->info("null work item!");
			return NULL;
		}
		spd::get("lg")->info("thread {}, loop {} - dequeued one item {}",
				getId(), i, (int)workItem);

		TCPStream* connection = workItem->GetStream();

		HandleConnection(connection);

		//delete item storage (will close TCPStream)
		delete workItem;
		spd::get("lg")->info("work item deleted.");
	}
	return NULL;
}

void TCPConsumerThread::HandleConnection(TCPStream* client)
{
	if(!client)
	{
		spd::get("lg")->info("null client connection!");
		return;
	}
	
	//client is not null
	spd::get("lg")->info("handle client {}, port {}", 
		client->getPeerIP(), client->getPeerPort());

	{
		// FIXME: obviously should be a loop here, for multiple read/write calls (request chunks or 
		// multiple requests/reply pairs).
		
		//read data from client
		// FIXME: discover that client closed connection.
		char buf_req[MAX_SIZE_REQ];
		unsigned bytes_req = client->Read(buf_req, MAX_SIZE_REQ);
		spd::get("lg")->info("read {} bytes from client.", bytes_req);
		spd::get("lg")->info("message: {}", buf_req);

		//echo request to client.
		client->Write(buf_req, bytes_req);
	}

	// when reached here, means transaction ended.
	// (connection closed outside this scope).
}


