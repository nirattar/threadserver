#include "threads/thread.h"
#include "work_queue/work_queue.h"
#include "network/tcp_stream.h"
#include "tcp_work_item.h"

class TCPConsumerThread : public Thread
{
public:
	TCPConsumerThread(WorkQueue<TCPWorkItem*>& queue) : m_queue(queue) {}

	virtual void * run();
	void HandleConnection(TCPStream* client);
	
private:
	WorkQueue<TCPWorkItem*>& m_queue;
	
};
