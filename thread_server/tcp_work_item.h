#ifndef TCP_WORK_ITEM_H
#define TCP_WORK_ITEM_H

#include "network/tcp_stream.h"

class TCPWorkItem
{
public:
	TCPWorkItem(TCPStream * stream) : m_stream(stream) {}
	virtual ~TCPWorkItem() { delete m_stream; }
	TCPStream * GetStream() {return m_stream;}
private:
	TCPStream * m_stream;
};

#endif //TCP_WORK_ITEM_H
