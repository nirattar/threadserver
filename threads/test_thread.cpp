/*
 * main.cpp
 * 
 * Copyright 2018 Nir Attar <nattar@ubuntu-nir>
 * 
 */

#include "thread.h"
#include "gtest/gtest.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

// Test class - prints the thread id
class IdPrintThread : public Thread 
{
	virtual void * run()
	{
		cout << "I am thread no. " << getId() << endl;
		return NULL;
	}
};

// Create 2 threads, join them
TEST(TestThread, CreateJoin)
{
	IdPrintThread t1;
	IdPrintThread t2;

	t1.start();
	t2.start();

	t1.join();
	t2.join();
}

// Create a thread without join.
// Should detach and cancel thread.
TEST(TestThread, CreateNoJoin)
{
	IdPrintThread t1;
	t1.start();
	
	//FIXME - omitting this sleep creates a segfault. 
	//probably a bug in destructor flow. (thread will get cancelled before it actually started)
	usleep(1000);
}

