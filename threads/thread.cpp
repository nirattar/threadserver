#include "thread.h"
#include "spdlog/spdlog.h"

/*
 * detach, join - illegal
 * detach, detach - unspecified
 * join, join - unspecified
 * join, detach - not needed 
 * */

namespace spd = spdlog;

Thread::Thread() : m_tid(0), m_running(false), m_detached(false)
{}

Thread::~Thread()
{
	//when thread is running, will call cancel to terminate thred.
	//if not already detached, sets as detached first.
	//(when thread not running, nothing to do.)
	if (m_running)
	{
		if (!m_detached)
		{
			detach();
		}
		pthread_cancel(m_tid);
	}
}

int Thread::start () 
{
	int result = pthread_create(&m_tid, NULL, Thread::runThread, this);
	if (result == 0) 
	{
		//Mark as running
		m_running = true;
		spd::get("lg")->info("started thread:{}",getId());
    }
    return result;
}

int Thread::join()
{
	//ensure thread is not already joined or detached.
	int result = -1;
	if (m_running && !m_detached)
	{
		result = pthread_join(m_tid, NULL);
	}
	if(result == 0)
	{
		m_detached = true;
	}
	return result;
}

int Thread::detach()
{	
	//ensure thread is not already joined or detached.
	int result = -1;
	if (m_running && !m_detached)
	{
		result = pthread_detach(m_tid);
	}
	if(result == 0)
	{
		m_detached = true;
	}
	return result;
}


void * Thread::runThread(void * arg)
{
	if(!arg)
	{
		return NULL;
	}
	
	return ((Thread *)arg)->run();
}

pthread_t Thread::getId() 
{
    return m_tid;
}
