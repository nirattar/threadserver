#include <pthread.h>

// Provides a thread abstraction

class Thread 
{
public:
	Thread();
	virtual ~Thread();
	
	// Interface with the thread.
	int start();
	int join();
	int detach();
	pthread_t getId();
	virtual void * run() = 0;
	
private:

	pthread_t m_tid;
	bool m_running;
	bool m_detached;

	/// This is the function that will be given to pthread (pthread_create), 
	/// @param arg: void pointer that can be cast as a Thread object.
	static void * runThread(void * arg);

};
