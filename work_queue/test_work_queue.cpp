
/*
 * main.cpp
 * 
 * Copyright 2018 Nir Attar <nattar@ubuntu-nir>
 * 
 */

#include "work_queue.h"
#include "thread.h"
#include <iostream>
#include <unistd.h>

using std::cout;
using std::endl;

int cnt_global = 0;

void SingleThreadedTest()
{
	WorkQueue<int> wqi;
	wqi.addItem(5);
	wqi.addItem(6);
	std::cout << wqi.removeItem() << std::endl;
	std::cout << wqi.removeItem() << std::endl;
	
	wqi.addItem(7);
	wqi.addItem(8);
	std::cout << wqi.removeItem() << std::endl;
	wqi.addItem(9);
	wqi.addItem(10);
	std::cout << wqi.removeItem() << std::endl;
	std::cout << wqi.removeItem() << std::endl;
	std::cout << wqi.removeItem() << std::endl;
	
	// Remove from empty - don't try!
	//std::cout << wqi.removeItem() << std::endl;

}

class ThreadProducer : public Thread
{
public:
	ThreadProducer(WorkQueue<int>& queue) : m_queue(queue) {}
	
	virtual void * run()
	{
		cout << "Producer started running !" << endl;
				
		//enqueue new items
		for(int i=0; i<10; i++)
		{
			m_queue.addItem(cnt_global++);
			usleep(100);
		}
		
		return NULL;
	}
	
private:
	WorkQueue<int>& m_queue;	
};

class ThreadConsumer : public Thread
{
public:
	ThreadConsumer(WorkQueue<int>& queue) : m_queue(queue) {}
	
	virtual void * run()
	{
		cout << "Consumer started running !" << endl;

		//dequeue items
		for (int i=0; i<5; i++)
		{
			cout << "dequeued item:" << m_queue.removeItem() << endl;
			usleep(100);
		}
		return NULL;
	}
	
private:
	WorkQueue<int>& m_queue;
	
};


//Test through Thread class abstraction
void MultiThreadedTest_1Producer_2Consumer()
{

	//one queue shared by all threads
	WorkQueue<int> wqi;

	ThreadProducer thread_producer(wqi);
	thread_producer.start();

	ThreadConsumer thread_consumer1(wqi);
	thread_consumer1.start();
	ThreadConsumer thread_consumer2(wqi);
	thread_consumer2.start();	
	
	//wait threads to finish.
	thread_consumer1.join();
	thread_consumer2.join();
	thread_producer.join();
}

int main(int argc, char **argv)
{
	
	//Single threaded test
	SingleThreadedTest();

	//Multi threaded test, 1 producer and 2 consumers	
	//Test through Thread class abstraction
	MultiThreadedTest_1Producer_2Consumer();

	
	return 0;
}

