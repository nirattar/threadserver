
template <typename T>
WorkQueue<T>::WorkQueue()
{
	pthread_mutex_init(&m_mutex, NULL);
	pthread_cond_init(&m_cond, NULL);
	
}

template <typename T>
WorkQueue<T>::~WorkQueue()
{
	pthread_mutex_destroy(&m_mutex);
	pthread_cond_destroy(&m_cond);

	// Clear all work items left in the queue (not consumed by workers).
	// FIXME: this assumes T is pointer type. 
	// need check for it (using type traits) or restrict it.
	m_list.remove_if(deleteAll);
}

template <typename T>
void WorkQueue<T>::addItem(T item)
{
	//acquire the lock (blocking)
    pthread_mutex_lock(&m_mutex);
    //critical section begin
    
	m_list.push_back(item);
    
    //debug
	spdlog::get("lg")->info("added item to queue: {}", (int)item);
    
    //signal that queue is non-empty
    pthread_cond_signal(&m_cond);

    //critical section end    
    pthread_mutex_unlock(&m_mutex);
    
}

template <typename T>
T WorkQueue<T>::removeItem()
{

	//acquire the lock (blocking)
    pthread_mutex_lock(&m_mutex);
    
    //critical section begin

	//re-evaluate if queue is empty. if it is, wait for signal.
	while(m_list.size()==0) 
	{
		pthread_cond_wait(&m_cond, &m_mutex);
	}
	
	//queue is guaranteed to be non-empty.
	
	T ret = m_list.front();
	m_list.pop_front();
	
    //critical section end
    pthread_mutex_unlock(&m_mutex);
    
    return ret;
}
