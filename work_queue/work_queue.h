#ifndef WORK_QUEUE_H
#define WORK_QUEUE_H

#include "spdlog/spdlog.h"
#include <pthread.h>
#include <list>

//T - work object

//Thread safe work queue
template <typename T> class WorkQueue 
{
public:
	WorkQueue();
	virtual ~WorkQueue();
	void addItem(T item);
	T removeItem();

	static bool deleteAll(T item)
	{ 
		delete item; 
		return true; 
	}

private:
	std::list<T> m_list;

	// Used to serialize access to list elements
	pthread_mutex_t m_mutex;

	//used to signal threads that list is not empty.
	pthread_cond_t m_cond;

};

// IMPLEMENTATION
#include "work_queue.cpp"

#endif //WORK_QUEUE_H
